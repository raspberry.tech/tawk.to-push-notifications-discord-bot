/*
ALL RIGHTS RESERVED
CREATED BY: Yusuf Ibn Saifullah
LinkedIn: https://www.linkedin.com/in/yusuf-ibn-saifullah-241a79190/
*/

import { MessageEmbed } from "discord.js";

export async function ticketEmbed(emailAddress: string, subject: string, ticketID: string, message: string, timestamp: Date){
    return new MessageEmbed()
    .setColor('#0099ff')
	.setTitle(`${subject}`)
	.setURL('https://dashboard.tawk.to/login')
    .setDescription(`${message}`)
    //.attachFiles(['FILELOCATION/FILENAME']) //uncomment if you want to use your own uploaded image asset on the server
    .setThumbnail('https://i.imgur.com/wSTFkRM.png') /*use URL to your image or replace URL with 'attachment://filename'.
    above line (.attachFiles) must be uncommented for this to work.*/
	.addFields(
        { name: 'Email Address:', value: `${emailAddress}` },
        { name: 'Ticket ID:', value: `${ticketID}`}
	)
	.setTimestamp(timestamp)
}
	

