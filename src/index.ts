/*
ALL RIGHTS RESERVED
CREATED BY: Yusuf Ibn Saifullah
LinkedIn: https://www.linkedin.com/in/yusuf-ibn-saifullah-241a79190/
*/

import express from "express";
import * as https from "https";
import helmet from "helmet";
import * as fs from "fs";
import { config } from "dotenv";
config();
import * as crypto from "crypto";
import * as discord from "discord.js";
import { ticketEmbed } from './bot/embeds';

const app = express();
app.use(express.json()) // for parsing application/json
app.use(helmet()); // adding Helmet to enhance your API's security

const webhookSecret = process.env.TAWK_WEBHOOK_SECRET; //tawk.to webhook secret
const domain = process.env.DOMAIN; //domain of API server
const port = process.env.PORT; //port to run API server on
let channelID = process.env.CHANNEL_ID; //channel ID of channel you want bot to post in
const clientToken = process.env.DISCORD_BOT_TOKEN; //discord bot token
const discordAdmin = process.env.DISCORD_ADMIN; //discord admin role
const prefix = process.env.DISCORD_PREFIX; //discord prefix
const watchingNow = process.env.DISCORD_WATCHING_NOW; //discord bot status

// const pathToPrivKey = process.env.PRIV_KEY_PATH; //uncomment if you want to specify the location to your keys
// const pathToFullKey = process.env.FULL_KEY_PATH;

let credentials = { //letsencrypt default location for keys
    key: fs.readFileSync(
      `/etc/letsencrypt/live/${domain}/privkey.pem`,
      "utf8"
    ),
    cert: fs.readFileSync(
      `/etc/letsencrypt/live/${domain}/fullchain.pem`,
      "utf8"
    ),
};

// let credentials = { //uncomment if using own https keys (not letsencrypt) and comment above credentials
//     key: fs.readFileSync(
//       `${pathToPrivKey}`,
//       "utf8"
//     ),
//     cert: fs.readFileSync(
//       `${pathToFullKey}`,
//       "utf8"
//     ),
// };

async function verifySignature (body: string, signature: string) { //verifies the signature of the body to your webhook key to make sure api request is valid
  try{
    const digest = crypto
        .createHmac('sha1', webhookSecret)
        .update(body)
        .digest('hex');
    return signature.substring(1, signature.length - 1) === digest.toString(); //needed because one has quotations, one doesn't.
  }
  catch(err){
    console.log("An error occurred when verifying signature: ", err)
    return false;
  }
};

app.post('/webhooks', async (req: express.Request, res: express.Response) => {
    try{
      console.log("1/3 - Received Ticket");
      
      let pass = await verifySignature(JSON.stringify(req.body), JSON.stringify(req.headers["x-tawk-signature"]));
      if (!pass) {
          // verification failed
          console.log("2/3 - Failed Verification");
          res.status(400).send("Error");
      }
      else{
        // verification success
        console.log("2/3 - Passed Verification");
        let emailAddress = req.body.requester.email;
        let subject = req.body.ticket.subject;
        let ticketID = req.body.ticket.id;
        let message = req.body.ticket.message;
        let timestamp = req.body.time;
        if(message.length > 300){
          let shortenedMessage = message.substring(0, 190);
          let ticket = await ticketEmbed(emailAddress, subject, ticketID, shortenedMessage, timestamp);
          sendMessage(ticket);
        }
        else{
          let ticket = await ticketEmbed(emailAddress, subject, ticketID, message, timestamp);
          sendMessage(ticket);
        }
        
        res.status(200).send("OK");
        console.log("3/3 - Completed Ticket");
      }
    }
    catch(err){
      console.log(err);
      res.status(400).send("Error");
    }
});

async function sendMessage(messageToBeSent: discord.MessageEmbed){
  try{
    let channelID = process.env.CHANNEL_ID; //channel ID of channel you want bot to post in
    const channel = client.channels.cache.get(channelID) as discord.TextChannel;
    channel.send(messageToBeSent);
  }
  catch(err){
    console.log("An error occurred when sending message: ", err)
  }
}

const httpsServer = https.createServer(credentials, app);
httpsServer.listen(port); //edit this to change port
console.info(`Listening on port ${port}`);

const client = new discord.Client(); //discord bot instance

client.login(clientToken);
client.on('ready', () => {
    console.info(`Logged in as ${client.user.tag}!`);
    const channel = client.channels.cache.get(channelID) as discord.TextChannel;
    channel.send("Bot is online");
    client.user.setActivity(`${watchingNow}`);
});

client.on('message', message => {
  if (!message.content.startsWith(prefix) || message.member.roles.cache.find((role) => role.name === `${discordAdmin}`)) return; //if prefix is not used and role is not correct, do not do anything.

  const args = message.content.slice(prefix.length).trim().split(' ');
  const command = args.shift().toLowerCase();
  if(command === "ping"){
    message.channel.send(`Websocket heartbeat: ${client.ws.ping}ms.`);
    message.channel.send('Pinging...').then(sent => {
      sent.edit(`Roundtrip latency: ${sent.createdTimestamp - message.createdTimestamp}ms`);
    });
  }
	else if(command === "restart"){
    process.exit();
  }
});